import { Module } from '@nestjs/common';
import { EpicModule } from './module/epic.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    EpicModule,
  ],
})
export class AppModule {}