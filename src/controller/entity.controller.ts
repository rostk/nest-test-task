import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { EpicDto } from 'src/dto';
import { Epic } from '../entity';
import { EpicService } from '../service/epic.service';

@Controller()
export class EpicController {
    constructor(private epicService: EpicService) {}


    @Get()
    getAllEpics(): Promise<Epic[]> {
        return this.epicService.getAllEpic();
    }

    @Post()
    @UsePipes(ValidationPipe)
    createEpic(@Body() dto: EpicDto): Promise<Epic>{
        return this.epicService.createEpic(dto);
    }

    @Get(':id')
    getEpicById(@Param('id', ParseIntPipe) id: number): Promise<Epic> {
        return this.epicService.findEpicById(id);
    }
    @Patch(':id')
    updateEpic(@Param('id', ParseIntPipe) id: number, @Body() dto: EpicDto): Promise<Epic>{
        return this.epicService.updateEpic(id, dto);
    }

    @Delete(':id')
    deleteEpicById(@Param('id', ParseIntPipe) id: number): Promise<string> {
        return this.epicService.deleteEpicById(id);
    }
}
