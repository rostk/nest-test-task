import { EpicDto } from "./epic.dto";
import { CycleDto } from "./cycle.dto";

export {
    EpicDto,
    CycleDto
};
