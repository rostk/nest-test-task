import { BaseEntity, Column, Entity, JoinColumn, OneToMany, PrimaryColumn } from "typeorm";
import { Epic } from './epic.entity';
@Entity()
export class Cycle extends BaseEntity{

    @PrimaryColumn()
    id: string;

    @Column()
    label: string;

    @Column()
    dateFrom: Date;

    @Column()
    dateTo: Date;

    @Column()
    tenantId: number;

    @OneToMany(type => Epic, epic => epic.cycle)
    @JoinColumn({name: 'tenantId', referencedColumnName: 'tenantId'})
    epic: Epic[];
}