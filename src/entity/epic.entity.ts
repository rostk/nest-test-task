import { Entity, PrimaryColumn, ManyToOne, Column, Generated, JoinColumn, BaseEntity } from "typeorm";
import { Cycle } from "./cycle.entity";

@Entity()
export class Epic extends BaseEntity {

    @PrimaryColumn()
    id: string;

    @Column()
    @Generated("uuid")
    uuid: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    tenantId: number;

    @ManyToOne(type => Cycle, cycle => cycle, { onDelete: 'CASCADE' })
    @JoinColumn({ name: 'tenantId' })
    cycle: Cycle;

}