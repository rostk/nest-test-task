import { Epic } from './epic.entity';
import { Cycle } from './cycle.entity';

export {
    Epic,
    Cycle
};
